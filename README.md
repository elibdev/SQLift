# SQLift

A simple wrapper around the SQLite C API adapted for idiomatic usage with swift.

## Usage

SQLift lets you create your own database type that will handle database operations
for your specific database.

It is intended to be used for when you know what queries you need in advance.
It is not designed for ad-hoc queries. 
This lets SQLift cache your queries when you open your database.

The library handles database initialization and transactions. 
You define queries. 

SQLite is untyped. 
SQLift only uses the binary blob interfaces to the C SQLite library.
Only `Data` is written to and read from the database.
Any encoding or decoding should be done in your own code.

```swift
struct User {
    let id: Int
    let name: String
}

struct MyDB: Database {
    let statements: []
    let db: Database
    init(path: String, statements: [Statement]) {
        db = open(path)
        db.exec("""
        create table if not exists users (
            id integer primary key,
            name
        )
        """)
        for query in queries {
            statements.append(sqlite_prepare(query.rawValue))
        }
    }
    func getUser(id: Int) -> User {
        let name = db.exec("select name from user where id=?1", id)
        return User(id: id, name: name)
    }
    func getUser(name: Int) -> User {
        let id = db.exec("select id from user where name=?1", name)
        return User(id: id, name: name)
    }
    func getUserIDs() -> [Int] {
        return db.exec("select id from users")
    }
    func createUser(name: String) -> User {
        return db.transact {
            db.exec("insert into users values (?1)", name)
            return db.getUser(name: name)
        }
    }
}
```

Or just embrace the untyped nature:

```swift
let schema = Schema("""
create table kv (
    key primary key,
    value
);
""")

let db = Database(schema)
db["a"] = "yes"
print(db["a"]) // "yes"
```

Or we can just expose a key value stores:

```swift
let db = Database("/some/file")
let me = db["users"]["eli"]
print(me) // "eli"
```

EventChain schema:

```sql
create table content (
    hash primary key,
    value
);
create table links (
    hash primary key,
    value
);
create table feed (
    id integer primary key,
    hash references content
);
```

Or we can just expose the blockchain wrapper around SQLite:

```swift
let chain = EventChain("/some/file")
events.count // 3
event[1] // "some value"
for event in events {
    print(e.body)
    print(e.id) // 1, 2, 3
    print(e.date) // timestamp of when event was logged
    print(e.hash) // SHA hash of the event body
}
db.hash // SHA hash representing the current state of the database
db.log() // append the event to the log
db.count // 4
db[4].body // 
db[DateInterval(start: Date.distantPast, end: Date()] // all events in the past
```
