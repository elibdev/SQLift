import XCTest
import SQLift

final class SQLiftTests: XCTestCase {
    func testVersion() {
        XCTAssert(sqliteVersion().hasPrefix("3"))
    }

    static var allTests = [
        ("testVersion", testVersion),
    ]
}
