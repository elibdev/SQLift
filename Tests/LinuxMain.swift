import XCTest

import SQLiftTests

var tests = [XCTestCaseEntry]()
tests += SQLiftTests.allTests()
XCTMain(tests)