// swift-tools-version:4.2
import PackageDescription

let package = Package(
    name: "SQLift",
    products: [
        .library(
            name: "SQLift",
            targets: ["SQLift"]
        ),
    ],
    targets: [
        .target(
            name: "SQLift",
            dependencies: ["CSQLite3"]),
        .systemLibrary(
            name: "CSQLite3",
            pkgConfig: "sqlite3",
            providers: [
                .brew(["sqlite3"]),
                .apt(["libsqlite3-dev"]),
            ]),
        .testTarget(
            name: "SQLiftTests",
            dependencies: ["SQLift"]),
    ]
)
